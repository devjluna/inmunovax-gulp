/**
 * Modules Dependencies
 */
window.jQuery = $ = require('jquery')
require('slimscroll')
require('fullpage')
require('easings')
require('owl')
require('bootstrap')

$(document).ready(function() {
	$('#fullpage').fullpage({
		menu: '#menu',
		anchors: ['firstPage', 'secondPage', '3rdPage'],
		autoScrolling: false
	});

  $('.textvacu').modal();

  $(".carousel-vacuna").owlCarousel({
    autoPlay: 6000,
    items: 4,
    navigation: true,
    navigationText: [
      "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>"
      ],
  });
 
  $(".carousel-clientes").owlCarousel({
    autoPlay: 6000,
    items: 4,
    navigation: true,
    navigationText: [
      "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>"
      ],
  });
  $(".carousel-convenios").owlCarousel({
    autoPlay: 6000,
    items: 3,
    navigation: true,
    navigationText: [
      "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>"
      ],
  });

  $('.clickVacuna').click(function(){
    var id = $(this).attr('id');

    if(id == '1'){
      $('.titleModal').html('FLUQUADRI');
      $('.descriptionModal').html('VACUNA TETRAVALENTE CONTRA EL VIRUS DE LA INFLUENZA');
      $('present_desc').html('1 Jeringa prellenada  monodosis 0.25 ml. + aguja.  Niños de 06 meses a 35 meses.');
      $('.forma_des').html('Suspensión inyectable');
      $('.dosis_des').html('Niños de 06 meses a 8 años de edad cuando es primera vez que se vacunan contra la influenza deben recibir 2  dosis con intervalo de 1 mes. A partir de los 9 años de edad es 1 dosis  0.50 ml.');
      $('.via_dm').html('Intramuscular, zona anterolareal del muslo en niños menores  y  región deltoides ( parte superior del brazo).');
      $('.textvacu').modal();
    }else
    if(id == '2'){
      $('.titleModal').html('EUVAX – B Pediátrico');
      $('.descriptionModal').html('VACUNA RECOMBINANTE CONTRA LA HEPATITIS B');
      $('present_desc').html('Caja por un frasco vial 0.5 ml. Adicional Jeringa con dos agujas.');
      $('.forma_des').html('Suspensión inyectable');
      $('.dosis_des').html('Una dosis pediátrica (recién nacidos, lactantes y niños de hasta 15 años de edad) es 0.5 ml que contiene 10ug (o mcg) de HBsAg. El esquema de inmunización consiste en tres dosis de vacuna administrada en el siguiente calendario: 1era dosis: En la fecha elegida. 2da dosis: Un mes después de la primera dosis. 3era dosis: 6 meses después de la primera dosis.');
      $('.via_dm').html('Intramuscular, zona anterolateral del muslo  en  niños menores y región deltoides (parte superior del brazo).');
      $('.textvacu').modal();
    }else
    if(id == '3'){
      $('.titleModal').html('IMOVAX POLIO');
      $('.descriptionModal').html('VACUNA ANTIPOLIOMELITICA (INACTIVADA)');
      $('present_desc').html('Caja con jeringa prellenada de vidrio x 0.5 ml.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('Primovacunaciòn:  A partir de las 6 semanas, aplicar 3 dosis con 1-2 meses de intervalo. En el adulto no vacunado, 2 dosis con intervalo de 1-2 meses. Refuerzos: En el niño: 1 dosis de refuerzo en el segundo año de vida. En el adulto: 1 dosis de refuerzo entre 8 a 12 años después de la 2da inyección.');
      $('.via_dm').html('Intramuscular en la región deltoides del  brazo  o el muslo anterolateral.');
      $('.textvacu').modal();
    }else
    if(id == '4'){
      $('.titleModal').html('ACT-HIB');
      $('.descriptionModal').html('VACUNA HAEMOPHILUS INFLUENZAE TIPO b (CONJUGADA)');
      $('present_desc').html('1 frasco vidrio con liofilizado + jeringa de vidrio con solvente.');
      $('.forma_des').html('Polvo y solvente para solución inyectable en jeringa prellenada.');
      $('.dosis_des').html('Antes de los 6 meses : 3 dosis con  1 o 2 meses de intervalo. Dosis de refuerzo:  1 año después de la tercera inyección. Entre los 6-12 meses :  2 dosis con un mes de intervalo seguidos con una inyección de refuerzo a los 18 meses. De 1 a 5 años :  1 sola dosis.');
      $('.via_dm').html('Intramuscular.');
      $('.textvacu').modal();
    }else
    if(id == '5'){
      $('.titleModal').html('AVAXIM 80 pediátrico');
      $('.descriptionModal').html('VACUNA INACTIVADA ADSORBIDA CONTRA LA HEPATITIS A');
      $('present_desc').html('Caja por 1 jeringa prellenada de 0.5 ml.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('El esquema de vacunación es de 0.5 ml por inyección. Entre los 12 meses y 15 años , 1 dosis. Se recomienda administrar una dosis de refuerzo de 6 a 18 meses después.');
      $('.via_dm').html('Intramuscular en la región deltoides ( Parte superior del brazo).');
      $('.textvacu').modal();
    }else
    if(id == '6'){
      $('.titleModal').html('');
      $('.descriptionModal').html('');
      $('present_desc').html('');
      $('.forma_des').html('');
      $('.dosis_des').html('');
      $('.via_dm').html('');
      $('.textvacu').modal();
    }else
    if(id == '7'){
      $('.titleModal').html('');
      $('.descriptionModal').html('');
      $('present_desc').html('');
      $('.forma_des').html('');
      $('.dosis_des').html('');
      $('.via_dm').html('');
      $('.textvacu').modal();
    }else
    if(id == '8'){
      $('.titleModal').html('HEXAXIM');
      $('.descriptionModal').html('VACUNA CONTRA LA DIFTERIA, TETANOS, LA TOSFERINA (ACELULAR,COMPUESTA), LA HEPATITIS B  (ADN RECOMBINANTE), LA POLIOMELITIS INACTIVADA) Y EL HAEMOPHILUS INFLUENZAE TIPO b (CONJUGADA), ADSORBIDA.');
      $('present_desc').html('Caja por 1 jeringa prellenada de 0.5 ml. + 2 agujas.');
      $('.forma_des').html('Suspensión inyectable en jeringa prellenada');
      $('.dosis_des').html('Primovacunaciòn con 3 inyecciones separadas por un intervalo de 4 semanas a partir de las 6 semanas de edad, seguidas de una inyección de refuerzo entre los 15 y 18 meses de vida según las recomendaciones.');
      $('.via_dm').html('Intramuscular, preferiblemente en la región anterolateral del muslo (tercio medio).');
      $('.textvacu').modal();
    }else
    if(id == '9'){
      $('.titleModal').html('PENTAXIM');
      $('.descriptionModal').html('VACUNA ADSORBIDA DIFTERIA, TETANICA,PERTUSICA ACELULAR, POLIMELITICA INACTIVADA Y VACUNA CONJUGADA  HAEMOPHILUS INFLUENZAE TIPO b.');
      $('present_desc').html('Caja por 1 jeringa prellenada de 0.5 ml. + 1 vial liofilizado.');
      $('.forma_des').html('Polvo liofilizado para reconstituir a suspensión inyectable.');
      $('.dosis_des').html('Primovacinaciòn con 3 inyecciones separadas por un intervalo de 1 a 2 meses a partir de los 2 meses de edad, seguidos de una inyección de refuerzo en el transcurso del segundo año de vida.');
      $('.via_dm').html('Intramuscular preferiblemente en l región anterolateral del muslo (tercio medio).');
      $('.textvacu').modal();
    }else
    if(id == '10'){
      $('.titleModal').html('TETRAXIM');
      $('.descriptionModal').html('VACUNA DIFTERICA, TETANICA, ADSORBIDA PERTUSICA ACELULAR Y POLIOMELITICA INACTIVADA.');
      $('present_desc').html('Caja  x 1 jeringa prellenada 0.5 ml.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('Primovacunaciòn a partir de los 2 meses de edad, con intervalo de 1 – 2 meses. Refuerzo durante el segundo año de vida y después  entre los 5 a 13 años de edad según recomendación.');
      $('.via_dm').html('Intramuscular en la zona anterolateral del muslo en lactantes y en el deltoides en niños de 5 a 13 años de edad.');
      $('.textvacu').modal();
    }else
    if(id == '11'){
      $('.titleModal').html('PREVENAR-13');
      $('.descriptionModal').html('VACUNA CONJUGADA NEUMOCOCICA DE POLISACARIDOS (ABSORBIDA)');
      $('present_desc').html('Caja x 1 jeringa prellenada 0.5 ml.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('Primovacunación: 3 inyecciones  separadas con un intervalo de  2 meses. A partir de las 6 semanas de edad , se recomienda una 4 dosis de refuerzo entre los 11  a 15 meses de edad.');
      $('.via_dm').html('Intramuscular en la zona anterolateral del muslo en lactantes y en el deltoide parte superior del  brazo  en  niños y adultos.');
      $('.textvacu').modal();
    }else
    if(id == '12'){
      $('.titleModal').html('ROTATEQ');
      $('.descriptionModal').html('VACUNA ORAL PENTAVALENTE DE VIRUS VIVO CONTRA ROTAVIRUS');
      $('present_desc').html('Solución 2 ml. prellenada en tubo plástico (LDPE) con tapa rosca (HDPE)');
      $('.forma_des').html('Suspensión  para administración oral.');
      $('.dosis_des').html('Indicado para niños desde las 6 semanas (1 ½ mes) de edad   hasta  26 semanas (6 meses ½.) 3 dosis con intervalo  mínimo de  1 meses, máximo hasta las 26 semanas.');
      $('.via_dm').html('Únicamente vía oral.');
      $('.textvacu').modal();
    }else
    if(id == '13'){
      $('.titleModal').html('MENACTRA');
      $('.descriptionModal').html('VACUNA CONJUGADA DE POLISACARIDOS BACTERINOS DE MENINGOCOCO GRUPOS A,C , Y YW-135 CON TOXOIDE  DIFTERICO.');
      $('present_desc').html('Vial monodosis ( 5 viales por caja) Vial monodosis ( caja por 1) Adicional jeringa con dos agujas.');
      $('.forma_des').html('Solución inyectable.');
      $('.dosis_des').html('El esquema de vacunación es de 0.5 ml.  por inyección. En menores de 2 años,  2 dosis  a partir de los 9 meses de edad con intervalo de 3 meses. En mayores de 2 años de edad hasta los 55 años, una sola dosis.');
      $('.via_dm').html('Intramuscular, preferiblemente en  la región del deltoides o del muslo antero lateral según la edad y la masa muscular del individuo.');
      $('.textvacu').modal();
    }else
    if(id == '14'){
      $('.titleModal').html('M M R');
      $('.descriptionModal').html('VACUNA DE VIRUS VIVOS ATENUADOS CONTRA EL SARAMPION, PAPERAS  RUBEOLA.');
      $('present_desc').html('Vacuna liofilizada + ampolla de solvente, se debe reconstituir con 0.5 ml de solvente(agua para preparaciones inyectables).');
      $('.forma_des').html('Solución inyectable obtenida por reconstitución del liofilizado con el solvente.');
      $('.dosis_des').html('Se suele aplicar una primera dosis a partir de los 12 meses de edad, Se recomienda una sgunda dosis entre los 4 a 6 años de edad.');
      $('.via_dm').html('Subcutánea.');
      $('.textvacu').modal();
    }else
    if(id == '15'){
      $('.titleModal').html('VARIVAX');
      $('.descriptionModal').html('VACUNA DE VIRUS VIVO ATENUADO CONTRA LA VARICELA');
      $('present_desc').html('1 caja  Fco  ampolla ( 0.5 ml.  luego de reconstituida)');
      $('.forma_des').html('Polvo liofilizado – No menos de 1350 UFP + 0.7 ml de agua para inyección.');
      $('.dosis_des').html('Está indicada  a partir  los 12 meses hasta 12 años 1 dosis. A partir de los 13 años en adelante 2 dosis con intervalo de 1-2 meses. Solo en circunstancias especiales (brotes)   se puede aplicar a niños desde 9 meses de edad.');
      $('.via_dm').html('Intramuscular o subcutánea.');
      $('.textvacu').modal();
    }else
    if(id == '16'){
      $('.titleModal').html('GARDASIL');
      $('.descriptionModal').html('VACUNA  RECOMBINANTE TETRAVALENTE CONTRA PAPILOMA VIRUS HUMANO (TIPOS 6,11,16,18)');
      $('present_desc').html('1 Jeringa prellenada 0.5 ml');
      $('.forma_des').html('Suspensión inyectable');
      $('.dosis_des').html('Están indicadas 3 dosis de acuerdo a las recomendaciones oficiales: 0-2-6 meses. 1° dosis la fecha elegida. 2° dosis a los 2 meses de la 1°. 3° dosis a los 6 meses de la 1°. Alternativamente en individuos de 9 a 13 años de edad,  puede ser administrada de acuerdo a un esquema de 2 dosis ( 0, 6 meses  ó   0, 12 meses). A partir de los 14 deben aplicarse S 3 dosis. Mujeres de 9 a 45 años. Varones de 9 a 26 años ');
      $('.via_dm').html('Intramuscular, parte deldoidea de la parte superior  del brazo  ó  zona anterolateral  del muslo.');
      $('.textvacu').modal();
    }else
    if(id == '17'){
      $('.titleModal').html('ADACEL');
      $('.descriptionModal').html('VACUNA ADSORBIDA DE TOXOIDES DE TETANOS Y DIFTERIA COMBINADA CON COMPONENTE ACELULAR  DE  PERTUSSIS.');
      $('present_desc').html('Caja x 1 vial de una dosis 0.5 ml. Adicional  jeringa con 2 agujas.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('1 inyección (0.5 ml.) desde los 4 años a más de edad.');
      $('.via_dm').html('Intramuscular, preferiblemente en la región deltoidea.');
      $('.textvacu').modal();
    }else
    if(id == '18'){
      $('.titleModal').html('AVAXIM  160U');
      $('.descriptionModal').html('VACUNA INACTIVADA ADSORBIDA CONTRA LA HEPATITIS A');
      $('present_desc').html('Caja x 1 Jeringa Prellenada por 1 dosis de 0.5 ml.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('En adolescentes desde los 16 años y adultos, 1 dosis de vacuna seguida de un refuerzo 6 a 12 meses despúes dela primera vacunación.');
      $('.via_dm').html('Intramuscular.');;
      $('.textvacu').modal();
    }else
    if(id == '19'){
      $('.titleModal').html('EUVAX B  adulto');
      $('.descriptionModal').html('VACUNA CONTRA LA HEPATITIS B, RECOMBINANTE');
      $('present_desc').html('Caja por un frasco vial 1 ml. Adicional jeringa con 2 agujas.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('Una dosis adulta ( a partir de 16 años) es 1.0 ml. Que contiene 20 ug(o mcg) de HBsAg. El esquema de inmunización consiste en 3 dosis de vacuna administrada en el siguiente calendario: 1ra dosis: en la fecha elegida. 2da dosis: 1 mes después de la primera dosis. 3ra dosis: 6 meses después de la primera dosis.');
      $('.via_dm').html('Intramuscular, en el músculo deltoides (parte superior del brazo).');
      $('.textvacu').modal();
    }else
    if(id == '20'){
      $('.titleModal').html('IMOVAX  dT adulto');
      $('.descriptionModal').html('VACUNA ANTIDIFTERICA Y ANTITETANICA ADSORBIDA');
      $('present_desc').html('Caja x 1 Jeringa prellenada por 1 dosis de 0.5 ml.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('En la primovacunación, se recomienda administrar 3 dosis sucesivas de 0.5 ml. Refuerzos:  Debe administrarse una sola dosis cada 10 años.');
      $('.via_dm').html('Intramuscular o subcutánea profunda.');
      $('.textvacu').modal();
    }else
    if(id == '21'){
      $('.titleModal').html('PNEUMO-23');
      $('.descriptionModal').html('VACUNA ANTINEUMOCOCCICA DE POLISACARIDOS');
      $('present_desc').html('Caja x 1 Jeringa prellenada por 1 dosis de 0.5 ml.');
      $('.forma_des').html('Solución inyectable.');
      $('.dosis_des').html('Primovacunación: Una inyección de 0.5 ml., a partir de los 2 años de edad en grupos de alto riesgo. Revacunación: Una inyección de 0.5 ml. Según recomendación médica.');
      $('.via_dm').html('Administración de preferencia por vía intramuscular, o subcutánea.');
      $('.textvacu').modal();
    }else
    if(id == '22'){
      $('.titleModal').html('STAMARIL');
      $('.descriptionModal').html('VACUNA CONTRA LA FIEBRE AMARILLA, ATENUADA');
      $('present_desc').html('Caja x 1 vial liofilizada + Jeringa prellenada de 0.5 ml de solvente.');
      $('.forma_des').html('Polvo Liofilizado para reconstruir a suspensión inyectable.');
      $('.dosis_des').html('1 sola inyección de 0.5 ml. En adultos y niños a partir de los 9 meses de edad, al menos 10 días antes de ponerse en riesgo de infección.  Se recomienda una inyección de refuerzo cada 10 años, según la reglamentación sanitaria internacional.');
      $('.via_dm').html('Preferiblemente subcutánea.');
      $('.textvacu').modal();
    }else
    if(id == '23'){
      $('.titleModal').html('TETAVAX');
      $('.descriptionModal').html('VACUNA ANTITETANICA ADSORBIDA');
      $('present_desc').html('Caja x 1 Jeringa prellenada con una dosis de 0.5 ml.');
      $('.forma_des').html('Suspensión inyectable.');
      $('.dosis_des').html('Vacunación primaria: 2 dosis sucesivas con 1 o 2 meses de intervalo, seguidas de un refuerzo administrado entre 6 a 12 meses después de la segunda dosis. Refuerzo : Cada 10 años, 1 dosis.');
      $('.via_dm').html('Intramuscular preferentemente  o  subcutánea.');
      $('.textvacu').modal();
    }else
    if(id == '24'){
      $('.titleModal').html('TYPHIM VI');
      $('.descriptionModal').html('VACUNA ANTITIFOIDICA POLISACARIDA');
      $('present_desc').html('Caja x 1 Jeringa prellenada con una dosis 0.5 ml.');
      $('.forma_des').html('Solución inyectable.');
      $('.dosis_des').html('1 inyección de 0.5 ml a partir de los dos años de edad. Se recomienda la revacunación cada 3 años, si se mantiene la exposicipon al riesgo.');
      $('.via_dm').html('Subcutánea o intramuscular.');
      $('.textvacu').modal();
    }else
    if(id == '25'){
      $('.titleModal').html('VERORAB');
      $('.descriptionModal').html('VACUNA ANTIRRABICA INACTIVADA PREPARADA EN CELULAS ');
      $('present_desc').html('Caja por 1 vial liofilizado + Jeringa prellenada con 0.5 ml de solvente');
      $('.forma_des').html('Polvo para reconstituir a suspensión inyectable.');
      $('.dosis_des').html('Vacuna preventiva o pre-exposición -Primovacunación, 3 inyecciones en los días 0, 7 y 28. En caso necesario, la inyección del día 28 podrá administrarse el día 21. -Primer refuerzo, después de 1 año. -Refuerzos, posteriores, cada 5 años. La vacunación post-exposición o curativa debe realizarse bajo control médico , dependerá si recibió o no dosis de pre-exposición. Si tiene vacuna < 5 años: Dos inyecciones en el día 0 y 3. Si tiene v acuna> 5 años o vacuna incompleta (protocolo Essen): 5 inyecciones en los días 0,3,7,14 y 28 con la administración de IgR si fuera necesario.');
      $('.via_dm').html('Intramuscular en la región deltoidea.');
      $('.textvacu').modal();
    }else{
      return false;
    }
  })
});